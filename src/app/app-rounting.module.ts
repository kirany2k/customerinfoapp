import{ NgModule } from '@angular/core';
import{ RouterModule,Routes, Route } from '@angular/router';

import { PaginationComponent } from '../app/pagination-component/pagination.component';
import { InfiniteComponent } from '../app/infinite-component/infinite.component';

const routes: Routes = [
    { path: '',redirectTo:'/pagination',pathMatch:'full'},
    { path: 'pagination', component: PaginationComponent},
    { path:'infinite',component: InfiniteComponent}
];

@NgModule({
    imports : [RouterModule.forRoot(routes)],
    exports : [RouterModule]
})

export class AppRountingModule {}

