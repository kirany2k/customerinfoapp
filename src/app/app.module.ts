import {NgModule} from "@angular/core";
import {BrowserModule} from "@angular/platform-browser";
import {FormsModule} from "@angular/forms";
import {HttpClientModule} from "@angular/common/Http";
import {AgGridModule} from "ag-grid-angular/main";
import {AppComponent} from "./app.component";
import { AppRountingModule } from "./app-rounting.module";
import { PaginationComponent } from './pagination-component/pagination.component';
import { InfiniteComponent } from '../app/infinite-component/infinite.component';
import { CustomerService } from './shared/customer.services';
import { SubmitButtonComponent } from './shared/submit-button.component';

@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        AppRountingModule,
        HttpClientModule,
        AgGridModule.withComponents(
            [
                SubmitButtonComponent
            ])
    ],
    declarations: [
        AppComponent,
        PaginationComponent,
        InfiniteComponent ,     
        SubmitButtonComponent 
    ],
    providers:[CustomerService],
    bootstrap: [AppComponent]
})
export class AppModule {
}
