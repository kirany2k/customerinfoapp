import { Component } from "@angular/core"; 
import { ICellRendererAngularComp } from "ag-grid-angular";
import { CustomerService } from "./customer.services";
import { catchError} from 'rxjs/operators';


@Component(
    { selector: 'child-cell', 
      template: `<span><button style="height: 20px" (click)="postRequest()" class="btn btn-info">Info</button></span>`, 
      styles: [`.btn {line-height: 0.5}`] }) 

export class SubmitButtonComponent implements ICellRendererAngularComp {

    public params: any;

    constructor(private _customerService : CustomerService){

    }

    agInit(params: any): void 
    { 
        this.params = params; 
    }
    
    public postRequest() 
    {
         let rowId = this.params.value;
         let status = this.params.data.status;
         alert("/api/submit?rowId="+ rowId +"&status="+ status); 
        this._customerService.postToApi(rowId,status).
        pipe(catchError(err => {
            console.log(err);
            return 'err';
        })).
        subscribe(response => {
            console.log(response);
        });
    }

   

   
    refresh(): boolean 
    { 
        return false; 
    }
}


